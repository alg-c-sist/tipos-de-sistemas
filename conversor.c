/*                                     Universidade Tecnológica Federal do Paraná
                                                  Campus Pato Branco
                                            Engenharia de Computação 2020/2
                                         Intrudução à Engenharia de Computação
                                                 Bruna Castro Fernandes
                                                Caio Junqueira de Souza
                                               Claudio Luiz Fischer Júnior
                                             Pedro Henrique De Souza Santiago                                       */

#include <stdio.h>      //biblioteca padrão
#include <stdlib.h>     //biblioteca padrão
#include <windows.h>    //necessária para usar gotoxy
#include <string.h>     //necessária para usar strlen
#include <math.h>       //necessária para usar potencia
#include <ctype.h>      //necessária para usar toupper

//funções
void gotoxy(int x, int y);          //mover o cursor para um ponto especifico
void TELA();                        //limpa a tela, imprime bordas e titulo

int TamString(int n, int base);         //recebe um valor decimal e uma base de conversão e retorna quantos caracteres terá a conversão
int ConvertBINtoDEC(char bin[]);        //recebe string com valor binario, retorna a conversão decimal
int ConvertOCTtoDEC(char oct[]);        //recebe string com valor octal, retorna a conversão decimal
int ConvertHEXtoDEC(char hex[]);        //recebe string com valor hexadecimal, retorna a conversão decimal
void ConvertDECtoBIN(int dec, int y);   //recebe o valor decimal, converte para binário e imprime o resultado na posição recebida
void ConvertDECtoOCT(int dec, int y);   //recebe o valor decimal, converte para octal e imprime o resultado na posição recebida
void ConvertDECtoHEX(int dec, int y);   //recebe o valor decimal, converte para hexadecimal e imprime o resultado na posição recebida

//variaveis
int menu=1, opcao, decimal, resto, deci, tam, expo, aux, i, resultDec;
char binario[90], octal[90], hexadecimal[90];

int main()
{
    while (menu==1)
    {
        TELA(); //menu principal
        do
        {
            gotoxy(5,7);
            printf("Informe a base do valor que deseja inserir");
            gotoxy(5,9);
            printf("1 - Bin%crio", 160);
            gotoxy(5,11);
            printf("2 - Octal");
            gotoxy(5,13);
            printf("3 - Decimal");
            gotoxy(5,15);
            printf("4 - Hexadecimal");
            gotoxy(5,23);
            printf("Op%c%co 0 [zero] para encerrar o programa", 135, 198);
            gotoxy(5,17);
            printf("Op%c%co: ", 135, 198);
            scanf("%d", &opcao);
            if (opcao<0 || opcao>4)
            {
                gotoxy(5,19);
                printf("Op%c%co inv%clida! Por favor, selecione op%c%co 1, 2, 3 ou 4", 135, 198, 160, 135, 198);
                gotoxy(12,17);
                printf("%c%c%c%c%c", 255, 255, 255, 255, 255);
            }
        }while (opcao<0 || opcao>4);
        //apos ler e validar a escolha do usuário, vai ate a opção solicitada
        switch (opcao)
        {
        case 1:
            //leitura do valor binario em uma string
            TELA();
            gotoxy(5,7);
            printf("Converter n%cmero bin%crio", 163, 160);
            gotoxy(5,9);
            printf("Informe o valor bin%crio: ", 160);
            fflush(stdin);
            scanf("%s", &binario);
            //conversão para decimal realizada atraves de função com retorno inteiro, resultado decimal utilizado para as demais conversões
            resultDec=ConvertBINtoDEC(binario);
            ConvertDECtoOCT(resultDec, 13);
            ConvertDECtoHEX(resultDec, 17);
            //impressão do resultado decimal que foi retornado e mensagem de pausa
            gotoxy(5,15);
            printf("Decimal     =   %d", resultDec);
            gotoxy(5,23);
            system("pause");
            break;

        case 2:
            //leitura do valor octal em uma string
            TELA();
            gotoxy(5,7);
            printf("Converter n%cmero octal", 163);
            gotoxy(5,9);
            printf("Informe o valor octal: ");
            fflush(stdin);
            scanf("%s", &octal);
            //conversão para decimal realizada atraves de função com retorno inteiro, resultado decimal utilizado para as demais conversões
            resultDec=ConvertOCTtoDEC(octal);
            ConvertDECtoBIN(resultDec, 13);
            ConvertDECtoHEX(resultDec, 17);
            //impressão do resultado decimal que foi retornado e mensagem de pausa
            gotoxy(5,15);
            printf("Decimal     =   %d", resultDec);
            gotoxy(5,23);
            system("pause");
            break;

        case 3:
            //leitura do valor decimal em um inteiro
            TELA();
            gotoxy(5,7);
            printf("Converter n%cmero decimal", 163);
            gotoxy(5,9);
            printf("Informe o valor decimal: ");
            scanf("%d", &decimal);
            //valor decimal enviado para as funções de conversão, resultados impressos pelas funções
            ConvertDECtoBIN(decimal, 13);
            ConvertDECtoOCT(decimal, 15);
            ConvertDECtoHEX(decimal, 17);
            //mensagem de pausa
            gotoxy(5,23);
            system("pause");
            break;

        case 4:
            //leitura do valor hexadecimal em uma string
            TELA();
            gotoxy(5,7);
            printf("Converter n%cmero hexadecimal", 163);
            gotoxy(5,9);
            printf("Informe o valor hexadecimal: ");
            fflush(stdin);
            scanf("%s", &hexadecimal);
            //conversão para decimal realizada atraves de função com retorno inteiro, resultado decimal utilizado para as demais conversões
            resultDec=ConvertHEXtoDEC(hexadecimal);
            ConvertDECtoBIN(resultDec, 13);
            ConvertDECtoOCT(resultDec, 15);
            //impressão do resultado decimal que foi retornado e mensagem de pausa
            gotoxy(5,17);
            printf("Decimal     =   %d", resultDec);
            gotoxy(5,23);
            system("pause");
            break;

        default:
            //menu recebe o valor 0 para sair do laço while e encerrar o programa
            menu=0;
            break;
        }
    }
    return 0;
}

int TamString(int n, int base)
{
    //realiza as divisões do numero decimal pela base até que o numero decimal seja menor que a base
    //conta qunatas divisões foram relizadas e com isso se obtem o tamanho do numero convertido para a base informada
    int cont=0;
    if (n<base)
    {
        return 0;
    }
    while (n>base-1)
    {
        n=n/base;
        cont++;
    }
    return cont;
}

int ConvertBINtoDEC(char bin[])
{
    //limpa o valor das variaveis e define o tamanho necessário para a string binario
    deci=0;
    expo=0;
    tam=strlen(bin);
    tam--;              //retira um pois a string começa em bin[0]
    //laço para percorrer a string binaria de trás para frente
    for (i=tam; i>=0; i--)
    {
        if (bin[i]=='1')            //quando for igua a 1 realizar a conversão e somar para gerar o resultado final
        {                           //não é necessário converter quando o numeor for 0 pois a multiplicação por 0 anula sua soma
            deci += pow(2,expo);
        }
        expo++;                     //aumenta o valor do expoente
    }
    return deci;                    //retorna a conversão para decimal
}

int ConvertOCTtoDEC(char oct[])
{
    //limpa o valor das variaveis e define o tamanho necessário para a string octal
    deci=0;
    expo=0;
    tam=strlen(oct);
    tam--; //retira um pois a string começa em oct[0]
    do
    {   //convertendo o valor char para inteiro e alocando em uma variável auxuliar
        switch (oct[tam])
        {
        case '1':
            aux=1;
            break;
        case '2':
            aux=2;
            break;
        case '3':
            aux=3;
            break;
        case '4':
            aux=4;
            break;
        case '5':
            aux=5;
            break;
        case '6':
            aux=6;
            break;
        case '7':
            aux=7;
            break;
        default:
            aux=0;
            break;
        }
        //realiza a conversão do numero e soma no total
        deci=deci+(aux*(pow(8,expo)));
        expo++;
        tam--;
    }while(tam>=0);

    return deci; //retorna a conversão
}

int ConvertHEXtoDEC(char hex[])
{
    //limpa o valor das variaveis e define o tamanho necessário para a string hexadecimal
    deci=0;
    expo=0;
    tam=strlen(hex);
    tam--;  //diminui um pois o vetor começa em hexa[0]
    //laço para colocar os caracteres em maiusculo
    for (i=0; i<=tam; i++)
    {
        hex[i]=toupper(hex[i]);
    }
    //Converter os valores da string de char para int
    do
    {
        switch (hex[tam])
        {
        case '1':
            aux=1;
            break;
        case '2':
            aux=2;
            break;
        case '3':
            aux=3;
            break;
        case '4':
            aux=4;
            break;
        case '5':
            aux=5;
            break;
        case '6':
            aux=6;
            break;
        case '7':
            aux=7;
            break;
        case '8':
            aux=8;
            break;
        case '9':
            aux=9;
            break;
        case 'A':
            aux=10;
            break;
        case 'B':
            aux=11;
            break;
        case 'C':
            aux=12;
            break;
        case 'D':
            aux=13;
            break;
        case 'E':
            aux=14;
            break;
        case 'F':
            aux=15;
            break;
        default:
            aux=0;
            break;
        }
        //realiza a conversão do numero e soma no total
        deci=deci+(aux*(pow(16,expo)));
        expo++;
        tam--;
    }while(tam>=0);

    return deci; //retorna a conversão para decimal
}

void ConvertDECtoBIN(int dec, int y)
{
    resto=0;                                    //limpa o valor de resto
    tam=TamString(dec, 2);                      //a função retorna o tamanho necessário para a string binária alocar a conversão
    char bin[tam+1];                            //aumenta o tamanho da string pois é necessário espaço par alocar o caractere delimitador '\0'
    bin[tam+1]='\0';                            //aloca o caractere delimitador no final da string
    //laço para realizar as divisões
    do
    {
        resto=dec%2;                            //resto recebe o resto da operação de divisão entre o valor decimal e a base 2
        if (dec<2) resto=dec;                   //caso o valor decimal seja menor que a base resto recebe o valor decimal
        switch(resto)                           //switch para converter os valores de inteiros para char e alocar na string
        {
        case 1:
            bin[tam]='1';
            break;

        default:
            bin[tam]='0';
            break;
        }
        tam--;
        dec=dec/2;
    }while (tam>=0);

    gotoxy(5,y);                                //move o cursor ate a linha informada
    printf("Bin%crio     =   %s", 160, bin);    //imprime o resultado na linha informada
}

void ConvertDECtoOCT(int dec, int y)
{
    resto=0;                                    //limpa o valor de resto
    tam=TamString(dec, 8);                      //a função retorna o tamanho necessário para a string octal alocar a conversão
    char oct[tam+1];                            //aumenta o tamanho da string pois é necessário espaço par alocar o caractere delimitador '\0'
    oct[tam+1]='\0';                            //aloca o caractere delimitador no final da string
    //laço para realizar as divisões
    do
    {
        resto=dec%8;                            //resto recebe o resto da operação de divisão entre o valor decimal e a base 8
        if (dec<8) resto=dec;                   //caso o valor decimal seja menor que a base resto recebe o valor decimal
        switch(resto)                           //switch para converter os valores de inteiros para char e alocar na string
        {
        case 1:
            oct[tam]='1';
            break;
        case 2:
            oct[tam]='2';
            break;
        case 3:
            oct[tam]='3';
            break;
        case 4:
            oct[tam]='4';
            break;
        case 5:
            oct[tam]='5';
            break;
        case 6:
            oct[tam]='6';
            break;
        case 7:
            oct[tam]='7';
            break;
        default:
            oct[tam]='0';
            break;
        }
        tam--;
        dec=dec/8;
    }while (tam>=0);

    gotoxy(5,y);                                //move o cursor ate a linha informada
    printf("Octal       =   %s", oct);          //imprime o resultado na linah informada
}

void ConvertDECtoHEX(int dec, int y)
{
    resto=0;                                    //limpa o valor de resto
    tam=TamString(dec, 16);                     //a função retorna o tamanho necessário para a string hexadecimal alocar a conversão
    char hex[tam+1];                            //aumenta o tamanho da string pois é necessário espaço par alocar o caractere delimitador '\0'
    hex[tam+1]='\0';                            //aloca o caractere delimitador no final da string
    //laço para realizar as divisões
    do
    {
        resto=dec%16;                           //resto recebe o resto da operação de divisão entre o valor decimal e a base 16
        if (dec<16) resto=dec;                  //caso o valor decimal seja menor que a base resto recebe o valor decimal
        switch(resto)                           //switch para converter os valores de inteiros para char e alocar na string
        {
        case 1:
            hex[tam]='1';
            break;
        case 2:
            hex[tam]='2';
            break;
        case 3:
            hex[tam]='3';
            break;
        case 4:
            hex[tam]='4';
            break;
        case 5:
            hex[tam]='5';
            break;
        case 6:
            hex[tam]='6';
            break;
        case 7:
            hex[tam]='7';
            break;
        case 8:
            hex[tam]='8';
            break;
        case 9:
            hex[tam]='9';
            break;
        case 10:
            hex[tam]='A';
            break;
        case 11:
            hex[tam]='B';
            break;
        case 12:
            hex[tam]='C';
            break;
        case 13:
            hex[tam]='D';
            break;
        case 14:
            hex[tam]='E';
            break;
        case 15:
            hex[tam]='F';
            break;
        default:
            hex[tam]='0';
            break;
        }
        tam--;
        dec=dec/16;
    }while (tam>=0);

    gotoxy(5,y);                                //move o cursor ate a linha informada
    printf("Hexadecimal =   %s", hex);          //imprime o resultado na linah informada
}

void gotoxy(int x, int y)
{
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),(COORD){x-1,y-1});
}

void TELA()
{
    system("cls");
    int lin, col;
    lin=2;
    //linha 1
    do{
        gotoxy(lin,2); printf("%c",223);
        lin++;
    }while(lin<79);
    //coluna 1
    col=2;
    do{
        gotoxy(79,col); printf("%c",219);
        col++;
    }while(col<25);
    //linha 2
    lin=79;
    do{
        gotoxy(lin,25); printf("%c",223);
        lin--;
    }while(lin>1);
    //coluna 2
    col=24;
    do{
        gotoxy(2,col); printf("%c",219);
        col--;
    }while(col>1);
    //titulo
    gotoxy(26,4);
    printf("|| Conversor de Bases ||");
}
